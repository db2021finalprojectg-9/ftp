﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class AReports : Form
    {
        public AReports()
        {
            InitializeComponent();
        }

        private void AReports_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
    
            label1.ForeColor = Theme.SecondaryColor;
            label3.ForeColor = Theme.SecondaryColor;
            label5.ForeColor = Theme.SecondaryColor;
       

        }

		private void textBox1_Validating(object sender, CancelEventArgs e)
		{
			
				if (!System.IO.File.Exists(textBox1.Text) && !System.IO.Directory.Exists(textBox1.Text))
				{
                textBox1.Focus();
                textBox1.SelectAll();
					errorProvider1.SetError(textBox1, "Invalid path. Please enter a valid path to a file or folder.");
				}
				else
				{
					errorProvider1.SetError(textBox1, "");
				}
			



		}

		private void textBox2_Validating(object sender, CancelEventArgs e)
		{
			string pattern = "^[A-Z][a-z]*$";
			Regex regex = new Regex(pattern);

			if (!regex.IsMatch(textBox2.Text))
			{
                textBox2.Focus(); 
				textBox2.Select(0, textBox2.Text.Length);
				errorProvider2.SetError(textBox2, "Invalid  name. Must start with a capital letter and contain no spaces.");
			}
			else
			{
				errorProvider2.SetError(textBox2, "");
			}
		}
	}
}
