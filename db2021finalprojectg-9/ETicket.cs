﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace db2021finalprojectg_9
{
    public partial class ETicket : Form
    {
        public ETicket()
        {
            InitializeComponent();
        }

        private void ETicket_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
            button2.BackColor = Theme.PrimaryColor;
            button2.ForeColor = Color.White;
            button2.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button2.FlatStyle = FlatStyle.Flat;
            button3.BackColor = Theme.PrimaryColor;
            button3.ForeColor = Color.White;
            button3.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button3.FlatStyle = FlatStyle.Flat;
            button4.BackColor = Theme.PrimaryColor;
            button4.ForeColor = Color.White;
            button4.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button4.FlatStyle = FlatStyle.Flat;
            

        }

		private void textBox1_Validating(object sender, CancelEventArgs e)
        {
			string pattern = "^[A-Z][a-z]*$";
            Regex regex = new Regex(pattern);
            if (!regex.IsMatch(textBox1.Text))
            {
                textBox1.Focus();
				textBox1.Select(0, textBox1.Text.Length);
				errorProvider2.SetError(textBox1, ". Must start with a capital letter and contain no spaces.");
			}
			else
			{
				errorProvider2.SetError(textBox1, "");
			}
		}

		private void txtMaker_Validating(object sender, CancelEventArgs e)
		{
			string pattern = "^[A-Z][a-z]*$";
			Regex regex = new Regex(pattern);

			if (!regex.IsMatch(txtMaker.Text))
			{
                txtMaker.Focus();
				txtMaker.Select(0, txtMaker.Text.Length);
				errorProvider1.SetError(txtMaker, "Must start with a capital letter and contain no spaces.");
			}
			else
			{
				errorProvider1.SetError(txtMaker, "");
			}
		}

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.AutoResizeColumns();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Ticket", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
