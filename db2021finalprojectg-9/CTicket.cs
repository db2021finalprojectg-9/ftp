﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class CTicket : Form
    {
        public CTicket()
        {
            InitializeComponent();
        }

        private void CTicket_Load(object sender, EventArgs e)
        {
            LoadTheme();
            comboBox1.Text = "Lahore";
            textBox3.Text = "karachi";
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
            D.ForeColor = Theme.SecondaryColor;
            A.ForeColor = Theme.SecondaryColor;
            B.ForeColor = Theme.SecondaryColor;
            C.ForeColor = Theme.SecondaryColor;
          

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Lahore")
            {
                textBox3.Text = "Karachi";
            }
            if (comboBox1.SelectedItem.ToString() == "Karachi")
            {
                textBox3.Text = "Lahore";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedItem.ToString()=="Lahore")
            {
                textBox1.Text ="2000";
                textBox2.Text ="3000";
            }
            else
            {
                textBox1.Text = "3000";
                textBox2.Text = "4000";
            }
        }
    }
}
