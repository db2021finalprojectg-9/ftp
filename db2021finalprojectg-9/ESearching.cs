﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class ESearching : Form
    {
        public ESearching()
        {
            InitializeComponent();
        }

        private void ESearching_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
            
        }

		private void txtId_Validating(object sender, CancelEventArgs e)
		{
			if (Regex.IsMatch(txtId.Text, @"^\S+$"))
			{
				txtId.SelectAll();
				errorProvider1.SetError(txtId, "Spaces are not allowed.");
                txtId.Focus();
			}
		}
	}
}
