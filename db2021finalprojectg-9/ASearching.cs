﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace db2021finalprojectg_9
{
    public partial class ASearching : Form
    {
        public ASearching()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }
     
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void ASearching_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
           
            label1.ForeColor = Theme.SecondaryColor;
            label3.ForeColor = Theme.SecondaryColor;
       

        }

		private void txtId_Validating(object sender, CancelEventArgs e)
        {
			string pattern = @"^\S+$";
            Regex regex = new Regex(pattern);
            if (!regex.IsMatch(txtId.Text))
            {
                txtId.Focus();
                txtId.Select(0, txtId.Text.Length);
				errorProvider1.SetError(txtId, "Invalid first name. Must start with a capital letter and contain no spaces.");
			}
			else
			{
				errorProvider1.SetError(txtId, "");
			}
		}
	}
}
