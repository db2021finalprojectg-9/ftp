﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace db2021finalprojectg_9
{
    public partial class EMaintainance : Form
    {
        public EMaintainance()
        {
            InitializeComponent();
        }

        private void EMaintainance_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
            button2.BackColor = Theme.PrimaryColor;
            button2.ForeColor = Color.White;
            button2.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button2.FlatStyle = FlatStyle.Flat;
            button3.BackColor = Theme.PrimaryColor;
            button3.ForeColor = Color.White;
            button3.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button3.FlatStyle = FlatStyle.Flat;
            button4.BackColor = Theme.PrimaryColor;
            button4.ForeColor = Color.White;
            button4.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button4.FlatStyle = FlatStyle.Flat;
       
         

        }

		private void textBox1_Leave(object sender, EventArgs e)
        {
            // Get the current text in the textbox
			string text = textBox1.Text;

			// Check if the input is not null or empty
			if (!string.IsNullOrEmpty(text))
			{
                // Check if the input is numeric
				if (!int.TryParse(text, out _))
				{
					textBox1.SelectAll();

                    // Show an error message using error provider 4 and set the focus on the textbox
					errorProvider1.SetError(textBox1, "Only numeric values are allowed.");
                    textBox1.Focus();
                }
				else
                {
                    // Clear any error message using error provider 4
					errorProvider1.SetError(textBox1, "");
				}
			}
			else
			{
				textBox1.SelectAll();

				textBox1.Focus();
				errorProvider1.SetError(textBox1, "Empty input is nott allowed.");

			}
		}

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.AutoResizeColumns();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Maintainance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
