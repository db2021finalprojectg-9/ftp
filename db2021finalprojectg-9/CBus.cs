﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class CBus : Form
    {
        public CBus()
        {
            InitializeComponent();
        }
        private void CBus_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'tMSDataSet.Bus' table. You can move, or remove it, as needed.
            LoadTheme();
            dataGridView1.DataSource = null;
            dataGridView1.AutoResizeColumns();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select * from Bus", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            // TODO: This line of code loads data into the 'tMSDataSet1.Bus' table. You can move, or remove it, as needed.
            // TODO: This line of code loads data into the 'tMSDataSet.Bus' table. You can move, or remove it, as needed.
            LoadTheme();
            // Set up the SQL connection
            using (SqlConnection conn = new SqlConnection("Data Source=(local);Initial Catalog=TMS;Integrated Security=True"))
            {
                // Open the connection
                conn.Open();

                // Set up the SQL query to get the count of the table
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM Bus", conn);

                // Execute the query and get the count
                int count = (int)cmd.ExecuteScalar();

                // Display the count in the TextBox
                txtId.Text = count.ToString();
            }
            // Query the database to retrieve the first model value
            string connectionString = "Data Source=(local);Initial Catalog=TMS;Integrated Security=True";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "SELECT TOP 1 Model FROM Bus ";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        // Display the retrieved model in the text box
                        textBox2.Text = result.ToString();
                    }
                    else
                    {
                        textBox2.Text = "";
                    }
                }
            }


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "SELECT Count(*) From Bus Where Id Not In (Select BusId From Schedule)";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        // Display the retrieved model in the text box
                        textBox1.Text = result.ToString();
                    }
                    else
                    {
                        textBox1.Text = "";
                    }
                }
            }




        }
        private void LoadTheme()
        {
            label4.ForeColor = Theme.SecondaryColor;
            label1.ForeColor = Theme.SecondaryColor;
            label2.ForeColor = Theme.SecondaryColor;
            label3.ForeColor = Theme.SecondaryColor;

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                // Retrieve the selected bus ID from the combo box
                int selectedBusId = Convert.ToInt32(((DataRowView)comboBox1.SelectedItem).Row["Id"]);
                // Query the database to retrieve the corresponding bus model
                string connectionString = "Data Source=(local);Initial Catalog=TMS;Integrated Security=True";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT Model FROM Bus WHERE Id = @busId";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@busId", selectedBusId);
                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            // Display the retrieved model in the text box
                            textBox2.Text = result.ToString();
                        }
                        else
                        {
                            textBox2.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }




        }
    }
    }

