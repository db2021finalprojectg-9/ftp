﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace db2021finalprojectg_9
{
	public partial class AAddEmployee : Form
	{
		public AAddEmployee()
		{
			InitializeComponent();
		}

		private void textBox1_Leave(object sender, EventArgs e)
		{ }

		private void txtFirstName_Validating(object sender, CancelEventArgs e)
		{
			
				

		}

		private void txtFirstName_Leave(object sender, EventArgs e)
		{
			
				txtFirstName_Validating(sender, new CancelEventArgs());
			

		}

		private void txtFirstName_Validating_1(object sender, CancelEventArgs e)
		{
			
			

		}

		private void txtFirstName_Leave_1(object sender, EventArgs e)
		{
			
			


		}

		private void textBox2_Leave(object sender, EventArgs e)
		{
			
		}
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
            button2.BackColor = Theme.PrimaryColor;
            button2.ForeColor = Color.White;
            button2.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button2.FlatStyle = FlatStyle.Flat;
            button3.BackColor = Theme.PrimaryColor;
            button3.ForeColor = Color.White;
            button3.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button3.FlatStyle = FlatStyle.Flat;
            button4.BackColor = Theme.PrimaryColor;
            button4.ForeColor = Color.White;
            button4.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button4.FlatStyle = FlatStyle.Flat;
            label4.ForeColor = Theme.SecondaryColor;
            label1.ForeColor = Theme.SecondaryColor;
            label2.ForeColor = Theme.SecondaryColor;
            label3.ForeColor = Theme.SecondaryColor;
            label5.ForeColor = Theme.SecondaryColor;
            label6.ForeColor = Theme.SecondaryColor;
            label7.ForeColor = Theme.SecondaryColor;
            label8.ForeColor = Theme.SecondaryColor;

        }

        private void AAddEmployee_Load(object sender, EventArgs e)
        {
			LoadTheme();
        }

		private void textBox2_Validating(object sender, CancelEventArgs e)
		{
				string pattern = "^[A-Z][a-z]*$";
				Regex regex = new Regex(pattern);
				if (!regex.IsMatch(textBox2.Text))
				{
				textBox2.Focus();
					textBox2.Select(0, textBox2.Text.Length);
					errorProvider2.SetError(textBox2, "Invalid first name. Must start with a capital letter and contain no spaces.");
				}
				else
				{
					errorProvider2.SetError(textBox2, "");
				}
			

		}

		private void textBox1_Validating(object sender, CancelEventArgs e)
		{
			
				string pattern = "^[A-Z][a-z]*$";
				Regex regex = new Regex(pattern);

				if (!regex.IsMatch(textBox1.Text))
				{
                textBox1.Focus();
                textBox1.Select(0, textBox1.Text.Length);
					errorProvider1.SetError(textBox1, "Invalid first name. Must start with a capital letter and contain no spaces.");
				}
				else
				{
					errorProvider1.SetError(textBox1, "");
				}
			

		}

		private void textBox3_Enter(object sender, EventArgs e)
		{
			
				//errorProvider3.SetError(textBox3, "");
			
		}

		private void textBox3_Validating(object sender, CancelEventArgs e)
		{
			
				string pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";
				Regex regex = new Regex(pattern);

				if (!regex.IsMatch(textBox3.Text))
				{
					textBox3.Focus();
					textBox3.Select(0, textBox3.Text.Length);
					errorProvider1.SetError(textBox3, "Invalid email address.");
				}
				else
				{
					errorProvider1.SetError(textBox3, "");
				}
			

		}

		private void textBox5_Validating(object sender, CancelEventArgs e)
		{
            string input = textBox5.Text; // replace "textBox1" with the name of your textbox

            if (input.Length >=5 &&
                input.Any(char.IsLetter) &&
                input.Any(char.IsDigit) &&
                input.Any(c => !char.IsLetterOrDigit(c)))
            {
				errorProvider5.SetError(textBox5, "");
            }
            else
            {
                // input is not valid, show error message and cancel the event
                errorProvider5.SetError(textBox5,"Input must be at least 5 characters long and contain a combination of alphabets, numbers, and special characters.");
                textBox5.Focus();
				textBox5.SelectAll();
            }
        }

		private void textBox6_Validating(object sender, CancelEventArgs e)
		{
			string pattern = "^[A-Z][a-z]*$";
			Regex regex = new Regex(pattern);

			if (!regex.IsMatch(textBox6.Text))
			{
				textBox6.Focus();
				textBox6.Select(0, textBox6.Text.Length);
				errorProvider6.SetError(textBox6, "Invalid Country name. Must start with a capital letter and contain no spaces.");
			}
			else
			{
				errorProvider6.SetError(textBox6, "");
			}
		}

        private void errorProvider7_RightToLeftChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.AutoResizeColumns();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Employee", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
           /* string Email = textBox1.Text;
            string query = "SELECT COUNT(*) FROM Employee WHERE Username = @Email";

            SqlCommand command = new SqlCommand(query, con);
            command.Parameters.AddWithValue("@Email", Email);
            int count = (int)command.ExecuteScalar();
            if (count > 0)
            {
                MessageBox.Show("Email Already Exists! Please Choose Another.")
                }
            else
            {
                email = true;
            }*/
        }
    }
}
