﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class EFeedback : Form
    {
        public EFeedback()
        {
            InitializeComponent();
        }

        private void EFeedback_Load(object sender, EventArgs e)
        {
            LoadTheme();
            dataGridView1.DataSource = null;
            dataGridView1.AutoResizeColumns();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Feedback", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        private void LoadTheme()
        {

        }

    }
}
