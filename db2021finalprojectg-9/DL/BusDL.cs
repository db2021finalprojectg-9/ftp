﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class BusDL
    {
        private List<BusBL> busInfo;

        internal List<BusBL> BusInfo { get => busInfo; set => busInfo = value; }

        public void addBus(BusBL bus)
        { busInfo.Add(bus); }

        public void deleteBus(BusBL bus)
        { busInfo.Remove(bus); }

        public List<BusBL> getAllBus()
        { return busInfo; }
    }
}
