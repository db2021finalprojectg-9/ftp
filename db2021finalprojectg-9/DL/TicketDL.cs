﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class TicketDL
    {
        private List<TicketBL> ticketInfo;

        internal List<TicketBL> TicketInfo { get => ticketInfo; set => ticketInfo = value; }

        public void addTicket(TicketBL ticket)
        { TicketInfo.Add(ticket); }

        public void deleteTicket(TicketBL ticket)
        { TicketInfo.Remove(ticket); }

        public List<TicketBL> getAllTicket()
        { return ticketInfo; }
    }
}
