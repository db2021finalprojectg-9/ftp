﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class EmpAttendanceDL
    {
        private List<EmpAttendanceBL> empAttendance;

        internal List<EmpAttendanceBL> EmpAttendance { get => empAttendance; set => empAttendance = value; }

        public void addAttendance(EmpAttendanceBL empAtt)
        { empAttendance.Add(empAtt); }

        public void deleteAttendance(EmpAttendanceBL empAtt)
        { empAttendance.Remove(empAtt); }

        public List<EmpAttendanceBL> showEmpAttendance()
        { return empAttendance; }
    }
}
