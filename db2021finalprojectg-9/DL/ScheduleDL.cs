﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class ScheduleDL
    {
        private List<ScheduleBL> scheduleInfo;

        internal List<ScheduleBL> ScheduleInfo { get => scheduleInfo; set => scheduleInfo = value; }

        public void addSchedule(ScheduleBL schedule)
        { scheduleInfo.Add(schedule); }

        public void deleteSchedule(ScheduleBL schedule)
        { scheduleInfo.Remove(schedule); }

        public List<ScheduleBL> getAllSchedule()
        { return scheduleInfo; }


    }
}
