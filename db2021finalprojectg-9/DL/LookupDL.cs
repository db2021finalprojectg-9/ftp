﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class LookupDL
    {
        private readonly string connectionString;
        private List<LookupBL> lookupList;

            public LookupDL(string connectionString)
            {
                this.connectionString = connectionString;
            }

        internal List<LookupBL> LookupList { get => lookupList; set => lookupList = value; }

       

        public LookupBL GetLookupDataById(int id)
            {
            LookupBL lookupData = LookupBL.Instance;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand("SELECT * FROM LookupTable WHERE id=@id", connection);
                    command.Parameters.AddWithValue("@id", id);
                    connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        lookupData.Role = (string)reader["role"];
                        lookupData.Name = (string)reader["name"];
                    }
                    else
                    {
                        throw new ArgumentException($"Lookup data not found for id {id}");
                    }
                }

                return lookupData;
            }

            public void UpdateLookupDataById(LookupBL lookup)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand("UPDATE LookupTable SET role=@role, name=@name WHERE id=@id", connection);
                    command.Parameters.AddWithValue("@role", lookup.Role);
                    command.Parameters.AddWithValue("@name", lookup.Name);
                    connection.Open();

                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected == 0)
                    {
                        throw new ArgumentException($"Lookup data not found for id Id");
                    }
                }
            }
     }
}
