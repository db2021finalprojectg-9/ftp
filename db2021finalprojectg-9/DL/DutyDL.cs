﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class DutyDL
    {
        private List<DutyBL> dutyInfo;

        internal List<DutyBL> DutyInfo { get => dutyInfo; set => dutyInfo = value; }

        public void addDuty(DutyBL duty)
        { dutyInfo.Add(duty); }

        public void deleteDuty(DutyBL duty)
        { dutyInfo.Remove(duty); }

        public List<DutyBL> getAllDuty()
        { return dutyInfo; }
    }
}
