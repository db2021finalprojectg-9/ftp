﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class CashierAttendanceDL
    {
        private List<CashierAttendanceBL> cashierAttendance;

        internal List<CashierAttendanceBL> CashierAttendance { get => cashierAttendance; set => cashierAttendance = value; }

        public void addCashier(CashierAttendanceBL cashierAtt)
        { cashierAttendance.Add(cashierAtt); }

        public void deleteCashier(CashierAttendanceBL cashierAtt)
        { cashierAttendance.Remove(cashierAtt); }

        public List<CashierAttendanceBL> showCashierAttendance()
        { return cashierAttendance; }
    }
}
