﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class MaintainanceDL
    {
        private List<MaintainanceBL> maintainanceInfo;

        internal List<MaintainanceBL> MaintainanceInfo { get => maintainanceInfo; set => maintainanceInfo = value; }


        public void addMaintainance(MaintainanceBL maintainance)
        { maintainanceInfo.Add(maintainance); }

        public void deleteMaintainance(MaintainanceBL maintainance)
        { maintainanceInfo.Remove(maintainance); }

        public List<MaintainanceBL> getAllMaintainance()
        { return maintainanceInfo; }
    }
}
