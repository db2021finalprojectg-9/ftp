﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{   
    class FeedbackDL
    {
        private List<FeedbackBL> feedbackInfo;

        internal List<FeedbackBL> FeedbackInfo { get => feedbackInfo; set => feedbackInfo = value; }
        
        public void addFeedback(FeedbackBL feeback)
        { FeedbackInfo.Add(feeback); }

        public void deleteFeedback(FeedbackBL feedback)
        { FeedbackInfo.Remove(feedback); }

        public List<FeedbackBL> getAllFeedbacks()
        { return FeedbackInfo; }

    }
}
