﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class EmployeeDL
    {
        private static List<EmployeeBL> employeeInfo = new List<EmployeeBL>();

        internal static List<EmployeeBL> EmployeeInfo { get => employeeInfo; set => employeeInfo = value; }

        public void addEmployee(EmployeeBL employee)
        { EmployeeInfo.Add(employee); }

        public void deleteEmployee(EmployeeBL employee)
        { EmployeeInfo.Remove(employee); }

        public List<EmployeeBL> getAllEmployee()
        { return employeeInfo; }
        public static EmployeeBL SignIn(EmployeeBL user,string A,string B)
        {
            string username = A; // replace with user input
            string password = B; // replace with user input
            var con = Configuration.getInstance().getConnection();
            // Construct the SQL query to retrieve the corresponding credentials from the database
            string query = "SELECT * FROM Employee WHERE Username = @username AND Password = @password And UserId=2";
            // Create a command object with the SQL query and parameters
            SqlCommand command1 = new SqlCommand(query, con);
            command1.Parameters.AddWithValue("@username", username);
            command1.Parameters.AddWithValue("@password", password);

            DataTable dataTable = new DataTable();

            // Open the database connection, fill the DataTable with the query results, and close the connection
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command1))
            {
                dataAdapter.Fill(dataTable);
            }

            // Check if the query returned any rows (i.e. if the credentials are valid)
            if (dataTable.Rows.Count > 0)
            {
                return user;
            }
            return null;
        }// end of 
    }
}
