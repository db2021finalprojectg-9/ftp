﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9.BL;

namespace db2021finalprojectg_9.DL
{
    internal class RouteDL
    {
        private List<RouteBL> itemsInfo;

        internal List<RouteBL> ItemsInfo { get => itemsInfo; set => itemsInfo = value; }

        public void addItem(RouteBL item)
        { ItemsInfo.Add(item); }

        public void deleteItems(RouteBL item)
        { ItemsInfo.Remove(item); }

        public List<RouteBL> getAllItems()
        { return ItemsInfo; }
    }
}
