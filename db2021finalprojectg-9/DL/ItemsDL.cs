﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class ItemsDL
    {
        private List<ItemsBL> itemsInfo;

        internal List<ItemsBL> ItemsInfo { get => itemsInfo; set => itemsInfo = value; }

        public void addItem(ItemsBL item)
        { ItemsInfo.Add(item); }

        public void deleteItems(ItemsBL item)
        { ItemsInfo.Remove(item); }

        public List<ItemsBL> getAllItems()
        { return ItemsInfo; }
    }
}
