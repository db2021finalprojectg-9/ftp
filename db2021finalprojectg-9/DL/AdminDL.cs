﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class AdminDL
    {
        private static List<AdminBL> adminInfo= new List<AdminBL>();

        internal static List<AdminBL> AdminInfo { get => adminInfo; set => adminInfo = value; }

        public static void addAdmin(AdminBL admin)
        { 
            adminInfo.Add(admin);
        }

        public void deleteAdmin(AdminBL admin)
        { adminInfo.Remove(admin); }

        public List<AdminBL> getAllAdmin()
        { return adminInfo; }
        public static AdminBL SignIn(AdminBL user,string A,string B)
        {
            string username = A; // replace with user input
            string password = B; // replace with user input
            var con = Configuration.getInstance().getConnection();
            // Construct the SQL query to retrieve the corresponding credentials from the database
            string query = "SELECT * FROM Admin WHERE Username = @username AND Password = @password And UserId=1";
            // Create a command object with the SQL query and parameters
            SqlCommand command1 = new SqlCommand(query, con);
            command1.Parameters.AddWithValue("@username", username);
            command1.Parameters.AddWithValue("@password", password);
            DataTable dataTable = new DataTable();
            // Open the database connection, fill the DataTable with the query results, and close the connection
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command1))
            {
                dataAdapter.Fill(dataTable);
            }

            // Check if the query returned any rows (i.e. if the credentials are valid)
            if (dataTable.Rows.Count > 0)
            {
                return user;
            }
            return null;
        }// end of 
    }
}
