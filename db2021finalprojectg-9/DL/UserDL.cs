﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class UserDL
    {
        private List<UserBL> usersInfo = new List<UserBL>();

        internal List<UserBL> UsersInfo { get => usersInfo; set => usersInfo = value; }

        public void AddUser(UserBL user)
        {
            UsersInfo.Add(user);
        }

        public void RemoveUser(UserBL user)
        {
            UsersInfo.Remove(user);
        }

        public List<UserBL> GetAllUsers()
        {
            return UsersInfo;
        }

    }
}
