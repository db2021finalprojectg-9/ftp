﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using db2021finalprojectg_9;

namespace db2021finalprojectg_9
{
    class CashierDL
    {
        private List<CashierBL> CashierInfo;

        internal List<CashierBL> CashierInfo1 { get => CashierInfo; set => CashierInfo = value; }

        public void addCashier(CashierBL cashier)
        { CashierInfo.Add(cashier); }

        public void deleteCashier(CashierBL cashier)
        { CashierInfo.Remove(cashier); }

        public List<CashierBL> getAllCashier()
        { return CashierInfo; }
    }
}

