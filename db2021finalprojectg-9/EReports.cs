﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace db2021finalprojectg_9
{
    public partial class EReports : Form
    {
        public EReports()
        {
            InitializeComponent();
        }

        private void EReports_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
          



        }

		private void txtId_Validating(object sender, CancelEventArgs e)
		{
			if (!System.IO.File.Exists(txtId.Text) && !System.IO.Directory.Exists(txtId.Text))
			{
				txtId.Focus();
				txtId.SelectAll();
				errorProvider1.SetError(txtId, "Invalid path. Please enter a valid path to a file or folder.");
			}
			else
			{
				errorProvider1.SetError(txtId, "");
			}
		}

		private void textBox1_Validating(object sender, CancelEventArgs e)
		{
			string pattern = "^[A-Z][a-z]*$";
			Regex regex = new Regex(pattern);

			if (!regex.IsMatch(textBox1.Text))
			{
				textBox1.Focus();
				textBox1.Select(0, textBox1.Text.Length);
				errorProvider2.SetError(textBox1, "Invalid  name. Must start with a capital letter and contain no spaces.");
			}
			else
			{
				errorProvider2.SetError(textBox1, "");
			}
		}
	}
}
