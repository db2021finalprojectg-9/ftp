﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
	public partial class Login : Form
	{
		public Login()
		{
			InitializeComponent();
		}

		private void button5_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void button5_Click_1(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.Hide();
			Form f =new SignUp();
			f.Show();
		}

        private void button1_Click(object sender, EventArgs e)
        {
			if (textBox1.Text != "" && textBox2.Text != "")
			{
				try
				{
					int a = 0;
					string username = textBox1.Text;
					string pass = textBox2.Text;
					ClientBL C = new ClientBL();
					C.Username = username;
					C.Password = pass;
					ClientBL N = ClientDL.SignIn(C,username,pass);
					if (N != null)
					{
						this.Hide();
						Form3 As = new Form3();
						As.Show();
					}
					else
					{
						a = a + 1;
					}
					AdminBL A = new AdminBL();
					A.Username = username;
					A.Password = pass;
					AdminBL M = AdminDL.SignIn(A,A.Username,A.Password);
					if (M != null)
					{
						this.Hide();
						Form2 A1 = new Form2();
						A1.Show();
					}
					else
					{
						a = a + 1;
					}

					EmployeeBL E = new EmployeeBL();
					E.Username= username;
					E.Password = pass;
					EmployeeBL K = EmployeeDL.SignIn(E,E.Username,E.Password);
					if (K != null)
					{
						this.Hide();
						Form1 A12 = new Form1();
						A12.Show();
					}
					else
					{
						a = a + 1;
					}
					if (a == 3)
					{
						MessageBox.Show("WRONG CREDENTIALS !");
					}

				}
				catch(Exception R)
				{
					MessageBox.Show(R.Message);
				}
			}
			else
			{
				MessageBox.Show("ENTER THE FIELDS FIRST !");
			}
        }
        private void Form1_Load(object sender, EventArgs e)
        {
		
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new SignUp();
            f.Show();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
			Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
			this.Hide();
			ForgotPass P = new ForgotPass();
			P.Show();
        }
        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public static string verificationCode = "0";

        private void button4_Click_1(object sender, EventArgs e)
        {

            string recipientEmail = textBox1.Text;
            if (IsValidEmail(recipientEmail))
            {
                // Generate verification code
                Random random = new Random();
                verificationCode = random.Next(100000, 999999).ToString();

                // Send verification code to recipient email
                try
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress("faizan2baig@gmail.com");
                    mail.To.Add(recipientEmail);
                    mail.Subject = "Email Verification Code";
                    mail.Body = "Your verification code is: " + verificationCode;

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("faizan2baig@gmail.com", "eivsurfhmaomzyfn");
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);

                    MessageBox.Show("Verification Code Sent To " + recipientEmail);
                    this.Hide();
                    ForgotPass forgotPassWindow = new ForgotPass();
                    forgotPassWindow.Show();
                    //this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid Email Address");
            }

        }
    }
}
