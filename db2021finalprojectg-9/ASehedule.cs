﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class ASehedule : Form
    {
        public ASehedule()
        {
            InitializeComponent();
        }

        private void ASehedule_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
            button2.BackColor = Theme.PrimaryColor;
            button2.ForeColor = Color.White;
            button2.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button2.FlatStyle = FlatStyle.Flat;
            button3.BackColor = Theme.PrimaryColor;
            button3.ForeColor = Color.White;
            button3.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button3.FlatStyle = FlatStyle.Flat;
            button4.BackColor = Theme.PrimaryColor;
            button4.ForeColor = Color.White;
            button4.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button4.FlatStyle = FlatStyle.Flat;
            label4.ForeColor = Theme.SecondaryColor;
            label1.ForeColor = Theme.SecondaryColor;
            label2.ForeColor = Theme.SecondaryColor;
            label3.ForeColor = Theme.SecondaryColor;
            label5.ForeColor = Theme.SecondaryColor;
            label6.ForeColor = Theme.SecondaryColor;
       

        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.AutoResizeColumns();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Schedule", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
