﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using db2021finalprojectg_9.BL;
using System.Data.Common;

namespace db2021finalprojectg_9
{
	public partial class SignUp : Form
	{
		public SignUp()
		{
			InitializeComponent();
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{

		}

		private void Form2_Load(object sender, EventArgs e)
		{
			// Disable the control box on the form
			this.ControlBox = false;

		}

		private void button5_Click(object sender, EventArgs e)
		{
			Application.Exit();	
		}

		private void button5_Click_1(object sender, EventArgs e)
		{
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.Close();	
			Form f =new Login();
			f.Show();
		}

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form f = new Login();
            f.Show();
        }

        private void button5_Click_2(object sender, EventArgs e)
        {
			Application.Exit();
        }

		private void textBox1_Validating(object sender, CancelEventArgs e)
		{
			string pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";
			Regex regex = new Regex(pattern);

			if (!regex.IsMatch(textBox1.Text))
			{
				textBox1.Focus();
				textBox1.Select(0, textBox1.Text.Length);
				errorProvider1.SetError(textBox1, "Invalid email address.");
			}
			else
			{
				errorProvider1.SetError(textBox1, "");
			}

		}

		private void textBox5_Validating(object sender, CancelEventArgs e)
		{
            string input = textBox5.Text; // replace "textBox1" with the name of your textbox

            if (input.Length >= 5 &&
                input.Any(char.IsLetter) &&
                input.Any(char.IsDigit) &&
                input.Any(c => !char.IsLetterOrDigit(c)))
            {
                errorProvider2.SetError(textBox5, "");
            }
            else
            {
                // input is not valid, show error message and cancel the event
                errorProvider2.SetError(textBox5, "Input must be at least 5 characters long and contain a combination of alphabets, numbers, and special characters.");
                textBox5.Focus();
                textBox5.SelectAll();
            }
        }

		private void button1_Click(object sender, EventArgs e)
		{
            bool email = false;
            var con = Configuration.getInstance().getConnection();
           

            if (textBoxName.Text != "" && textBox1.Text != "" && textBox5.Text != "" && comboBox1.SelectedItem.ToString() != "")
            {
                string Email = textBox1.Text;
                string query = "SELECT COUNT(*) FROM Client WHERE Username = @Email";
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@Email", Email);
                int count = (int)command.ExecuteScalar();
                if (count > 0)
                {
                    MessageBox.Show("Email Already Exists! Please Choose Another.");
                }
                else
                {
                    email = true;
                }
                if (email == true)
                {
                    Random rand = new Random();
                    string phoneNumber = "03";
                    string Role = "Client";
                    // Generate the remaining 9 digits randomly
                    for (int i = 0; i < 5; i++)
                    {
                        phoneNumber += rand.Next(0, 9).ToString();
                    }
                    SqlCommand cmd = new SqlCommand("Select Id From Users Where Role=@Client", con);
                    cmd.Parameters.AddWithValue("@Client", Role);
                    int m = (int)cmd.ExecuteScalar();
                    ClientBL C = new ClientBL();
                    C.Username = textBox1.Text;
                    C.Password = textBox5.Text;
                    C.FirstName = textBoxName.Text;
                    C.LastName = textBoxName.Text;
                    C.Contact = int.Parse(phoneNumber);
                    C.UserId = m;
                    C.Country = "Pakistan";
                    ClientDL.AddInList(C);
                    SqlCommand cmd1 = new SqlCommand("Insert into Client values (@UserId,@FirstName,@Contact,@LastName,@Username,@Country,@Password)", con);
                    cmd1.Parameters.AddWithValue("@UserId", m);
                    cmd1.Parameters.AddWithValue("@FirstName", textBoxName.Text);
                    cmd1.Parameters.AddWithValue("@Contact", int.Parse(phoneNumber));
                    cmd1.Parameters.AddWithValue("@LastName", textBoxName.Text);
                    cmd1.Parameters.AddWithValue("@Username", textBox1.Text);
                    cmd1.Parameters.AddWithValue("@Country", "Pakistan");
                    cmd1.Parameters.AddWithValue("@Password", textBox5.Text);
                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("SUCCESSFULL !");
                    this.Hide();
                    Login L = new Login();
                    L.Show();
                }
            }
            else
            {
                MessageBox.Show("ENTER THE FIELDS FIRST !");
            }
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
