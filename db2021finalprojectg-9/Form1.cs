﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class Form1 : Form
    {
        private Button currentButton;
        private Random random;
        private int tempIndex;
        public string text ;
        int len = 0;
        private Form activeForm;
        public Form1()
        {
            InitializeComponent();
            random = new Random();
        }
        private void OpenChildForm(Form childForm, object btnSender)
        {
            if (activeForm != null)
                activeForm.Close();
            ActivateButton(btnSender);
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelChild.Controls.Add(childForm);
            this.panelChild.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void Form1_Resize(object sender, EventArgs e)
        {            
               
            
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            text = lblAni.Text;
            lblAni.Text = "";
            timer1.Start();
 
        }
        private Color SelectThemeColor()
        {
            int index = random.Next(Theme.ColorList.Count);
            while (tempIndex == index)
            {
                index = random.Next(Theme.ColorList.Count); 
            }
            tempIndex = index;
            string color = Theme.ColorList[index];
            return ColorTranslator.FromHtml(color);
        }
        private void ActivateButton(object btnSender)
        {
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    DisableButton1();
                    Color color = SelectThemeColor();
                    currentButton = (Button)btnSender;
                    currentButton.BackColor = color;
                    panelLogo.BackColor = color;
                    panel11.BackColor = color;
                    currentButton.ForeColor = Color.White;
                    currentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    Theme.PrimaryColor = color;
                    Theme.SecondaryColor = Theme.ChangeColorBrightness(color, -0.3);

                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
        }

        private void DisableButton1()
        {
           
            foreach (Control previousBtn in panelMenu.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = Color.FromArgb(51, 51, 76);
                    previousBtn.ForeColor = Color.Gainsboro;
                    previousBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
        }
        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void btnRoute_Click(object sender, EventArgs e)
        {
            //ActivateButton(sender);

        }

        private void btnSchedule_Click(object sender, EventArgs e)
        {
            //ActivateButton(sender);

        }

        private void btnMaintainance_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);

        }

        private void btnAttendance_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);

        }

        private void btnAssignDuty_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);

        }

        private void btnTickets_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);

        }

        private void btnFeedback_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);

        }

        private void btnReports_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);

        }

        private void btnItems_Click(object sender, EventArgs e)
        {
            //ActivateButton(sender);

        }

        private void Logo_Click(object sender, EventArgs e)
        {

        }

        private void btnLuggage_Click(object sender, EventArgs e)
        {
           // ActivateButton(sender);
        }

        private void panelLogo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnBus_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnRoute_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnSchedule_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnMaintainance_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnAttendance_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnAssignDuty_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnTickets_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnFeedback_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnReports_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnLuggage_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }

        private void btnBus_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new EBus(), sender);
            label2.Text = "BUS INFORMATION";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;


        }

        private void btnSchedule_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new ESchedule(), sender);
            label2.Text = "CURRENT SCHEDULES";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;

        }

        private void btnRoute_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new ERoute(), sender);
            label2.Text = "ROUTE INFORMATION";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;
        }

        private void btnAttendance_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new EAttendance(), sender);
            label2.Text = "MARK ATTENDANCE"; 
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;
        }

        private void btnAssignDuty_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new EAssignDuty(), sender);
            label2.Text = "ASSIGN DUTY";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;

        }

        private void btnMaintainance_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new EMaintainance(), sender);
            label2.Text = "BUS MAINTAINANCE";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;

        }

        private void btnTickets_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new ETicket(), sender);
            label2.Text = "BUS TICKETS";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;
        }

        private void btnLuggage_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new ELuggage(), sender);
            label2.Text = "MANAGE LUGGAGE";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;

        }

        private void btnReports_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new EReports(), sender);
            label2.Text = "REPORT ANALYSIS";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;

        }

        private void btnSearch_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new ESearching(), sender);
            ActivateButton(sender);
            label2.Text = "SEARCH ITEMS";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;

        }

        private void btnFeedback_Click_2(object sender, EventArgs e)
        {
            OpenChildForm(new EFeedback(), sender);
            label2.Text = "CUSTOMER'S FEEDBACK";
            label2.Anchor = AnchorStyles.None;
            label2.Dock = DockStyle.None;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (len<text.Length)
            {
                lblAni.Text = lblAni.Text + text.ElementAt(len);
                len++;
            }
            

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (activeForm != null)
                activeForm.Close();
            Reset();
        }
        private void Reset()
        {
            DisableButton1();
            label2.Text = "HOME";
        }

        private void Form1_Resize_1(object sender, EventArgs e)
        {
            if (this.Width < 600) // adjust font size if the form width is less than 400 pixels
            {
                lblAni.Visible = false;


            }
            else
            {
                lblAni.Visible = true;

            }

        }
    }
}
