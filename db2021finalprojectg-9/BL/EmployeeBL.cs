﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class EmployeeBL
    {
        private string firstName;
        private string password;
        private string lastName;
        private string country;
        private string username;
        private int userId;
        private string city;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string City { get => city; set => city = value; }
        public string Country { get => country; set => country = value; }
        public string Username { get => username; set => username = value; }
        public int UserId { get => userId; set => userId = value; }
        public string Password { get => password; set => password = value; }
    }
}
