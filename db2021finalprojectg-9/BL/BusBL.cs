﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class BusBL
    {
        private string model;
        private string maker;
        private int vipSeats;
        private int normalSeats;
        private int totalSeats;
        public string Model { get => model; set => model = value; }
        public string Maker { get => maker; set => maker = value; }
        public int VipSeats { get => vipSeats; set => vipSeats = value; }
        public int NormalSeats { get => normalSeats; set => normalSeats = value; }
        public int TotalSeats { get => totalSeats; set => totalSeats = value; }
    }
}
