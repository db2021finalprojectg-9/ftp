﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9.BL
{
    class BinaryTree
    {
        public Node Root;

        public BinaryTree()
        {
            Root = null;
        }

        public void Insert(int data)
        {
            Node newNode = new Node(data);

            if (Root == null)
            {
                Root = newNode;
                return;
            }

            Node current = Root;
            Node parent = null;

            while (current != null)
            {
                parent = current;
                if (data < current.Data)
                {
                    current = current.Left;
                }
                else
                {
                    current = current.Right;
                }
            }

            if (data < parent.Data)
            {
                parent.Left = newNode;
            }
            else
            {
                parent.Right = newNode;
            }
        }
        public bool Delete(int data)
        {
            Node current = Root;
            Node parent = null;
            bool isLeftChild = false;

            while (current != null && current.Data != data)
            {
                parent = current;

                if (data < current.Data)
                {
                    current = current.Left;
                    isLeftChild = true;
                }
                else
                {
                    current = current.Right;
                    isLeftChild = false;
                }
            }

            if (current == null)
            {
                return false;
            }

            if (current.Left == null && current.Right == null)
            {
                if (current == Root)
                {
                    Root = null;
                }
                else if (isLeftChild)
                {
                    parent.Left = null;
                }
                else
                {
                    parent.Right = null;
                }
            }

            else if (current.Right == null)
            {
                if (current == Root)
                {
                    Root = current.Left;
                }
                else if (isLeftChild)
                {
                    parent.Left = current.Left;
                }
                else
                {
                    parent.Right = current.Left;
                }
            }
            else if (current.Left == null)
            {
                if (current == Root)
                {
                    Root = current.Right;
                }
                else if (isLeftChild)
                {
                    parent.Left = current.Right;
                }
                else
                {
                    parent.Right = current.Right;
                }
            }
            // Case 3: Node has two children
            else
            {
               Node successor = GetSuccessor(current);

                if (current == Root)
                {
                    Root = successor;
                }
                else if (isLeftChild)
                {
                    parent.Left = successor;
                }
                else
                {
                    parent.Right = successor;
                }

                successor.Left = current.Left;
            }

            return true;
        }

        private Node GetSuccessor(Node node)
        {
            Node parentOfSuccessor = node;
            Node successor = node;
            Node current = node.Right;

            while (current != null)
            {
                parentOfSuccessor = successor;
                successor = current;
                current = current.Left;
            }

            if (successor != node.Right)
            {
                parentOfSuccessor.Left = successor.Right;
                successor.Right = node.Right;
            }

            return successor;
        }

    }

}
