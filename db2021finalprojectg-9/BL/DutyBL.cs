﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    abstract class DutyBL
    {
        protected string role;
        protected DateTime date;
        protected int busId;

        public string Role { get => role; set => role = value; }
        public DateTime Date { get => date; set => date = value; }
        public int BusId { get => busId; set => busId = value; }

        public abstract void getRole();
    }
}
