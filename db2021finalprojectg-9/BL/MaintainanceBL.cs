﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class MaintainanceBL
    {
        private static MaintainanceBL instance;

        private int price;
        private int busId;
        private float avgPrice;
        private DateTime date;

        public int Price { get => price; set => price = value; }
        public int BusId { get => busId; set => busId = value; }
        public float AvgPrice { get => avgPrice; set => avgPrice = value; }
        public DateTime Date { get => date; set => date = value; }

        private MaintainanceBL()
        {
           
        }

        public static MaintainanceBL Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MaintainanceBL();
                }
                return instance;
            }
        }
    }
}
