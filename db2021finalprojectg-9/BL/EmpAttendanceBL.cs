﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class EmpAttendanceBL
    {
        private int employeeId;
        private string status;

        public int EmployeeId { get => employeeId; set => employeeId = value; }
        public string Status { get => status; set => status = value; }
    }
}
