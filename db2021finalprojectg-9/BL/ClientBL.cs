﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class ClientBL
    {
        private string firstName;
        private string lastName;
        private string username;
        private string country;
        private string password;
        private int contact;
        private int userId;
        public ClientBL()
        {

        }

        public ClientBL( string firstName, string lastName, string username, string password, int contact, int userId)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.username = username;
            this.password = password;
            this.contact = contact;
            this.userId = userId;
        }

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public int Contact { get => contact; set => contact = value; }
        public int UserId { get => userId; set => userId = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Country { get => country; set => country = value; }
    }
}
