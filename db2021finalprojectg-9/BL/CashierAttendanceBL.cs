﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class CashierAttendanceBL
    {
        private int cashierId;
        private string status;

        public int CashierId { get => cashierId; set => cashierId = value; }
        public string Status { get => status; set => status = value; }
    }
}
