﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class ScheduleBL
    {
        private static ScheduleBL instance;
        private int routeId;
        private int busId;
        private int empId;
        private DateTime date;

        public int RouteId { get => routeId; set => routeId = value; }
        public int BusId { get => busId; set => busId = value; }
        public int EmpId { get => empId; set => empId = value; }
        public DateTime Date { get => date; set => date = value; }

        private ScheduleBL() { }
        public static ScheduleBL GetInstance()
        {
            if (instance == null)
            {
                instance = new ScheduleBL();
            }
            return instance;
        }
    }
}
