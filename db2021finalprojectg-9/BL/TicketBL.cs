﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class TicketBL
    {
        private int clientId;
        private int busId;
        private int routeId;
        private int price;
        private DateTime departure;
        private DateTime arrival;
        private string destination;
        private int scehduleId;

        public int ClientId { get => clientId; set => clientId = value; }
        public int BusId { get => busId; set => busId = value; }
        public int RouteId { get => routeId; set => routeId = value; }
        public int Price { get => price; set => price = value; }
        public DateTime Departure { get => departure; set => departure = value; }
        public DateTime Arrival { get => arrival; set => arrival = value; }
        public string Destination { get => destination; set => destination = value; }
        public int ScehduleId { get => scehduleId; set => scehduleId = value; }
    }
}
