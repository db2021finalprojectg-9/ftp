﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9.BL
{
    class Queue
    {
        private int[] arr;
        private int size;
        private int rear = -1;

        Queue(int n)
        {
            arr = new int[n];
            size = n;
        }
        public bool IsEmpty()
        {
            return rear == -1;

        }
        public void Enqueue(int data)
        {
            if(rear==size-1)
            {
                return;
            }
            rear++;
            arr[rear] = data;
        }
        public int Dequeue()
        {
            if(IsEmpty())
            {
                return -1;
            }
            int front = arr[0];
            for(int i = 0; i < rear; i++)
            {
                arr[i] = arr[i+1];
            }
            rear--;

            return front;
        }
       
        public int peek()
        {
            if (IsEmpty())
            {
                return -1;
            }
            return arr[0];
        }
        public int Size()
        {
            return rear + 1;
        }
        public void clear()
        {
            for(int i=0;i<=rear;i++)
            {
                arr[i] = 0;
            }
            rear = -1;
        }
    }
}
