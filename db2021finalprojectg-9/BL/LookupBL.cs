﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class LookupBL
    {
        private static LookupBL instance;

        private string role;
        private string name;

        public string Role { get => role; set => role = value; }
        public string Name { get => name; set => name = value; }

        private LookupBL()
        {

        }
        public static LookupBL Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LookupBL();
                }
                return instance;
            }
        }
    }
}
