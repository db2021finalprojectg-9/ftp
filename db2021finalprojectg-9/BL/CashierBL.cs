﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class CashierBL
    {
        private string firstName;
        private string lastName;
        private string city;
        private string country;
        private int contact;
        private int userId;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string City { get => city; set => city = value; }
        public string Country { get => country; set => country = value; }
        public int Contact { get => contact; set => contact = value; }
        public int UserId { get => userId; set => userId = value; }
    }
}
