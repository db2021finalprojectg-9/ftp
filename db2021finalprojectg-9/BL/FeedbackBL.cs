﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class FeedbackBL
    {
        private int rating;
        private string advice;
        private int clientId;

        public int Rating { get => rating; set => rating = value; }
        public string Advice { get => advice; set => advice = value; }
        public int ClientId { get => clientId; set => clientId = value; }
    }
}
