﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    class ItemsBL
    {
        private float weight;
        private int busId;

        public float Weight { get => weight; set => weight = value; }
        public int BusId { get => busId; set => busId = value; }
    }
}
