﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9
{
    abstract class UserBL
    {
        protected string role;
        protected int salary;

        public string Role { get => role; set => role = value; }
        public int Salary { get => salary; set => salary = value; }

        public abstract void getRole();
    }
}
