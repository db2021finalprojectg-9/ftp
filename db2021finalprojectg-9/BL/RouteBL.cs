﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db2021finalprojectg_9.BL
{
    internal class RouteBL
    {
        private int distance;
        private string direction;
        private string start;
        private string end;

        public int Distance { get => distance; set => distance = value; }
        public string Direction { get => direction; set => direction = value; }
        public string Start { get => start; set => start = value; }
        public string End { get => end; set => end = value; }
    }
}
