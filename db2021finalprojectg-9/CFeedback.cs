﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db2021finalprojectg_9
{
    public partial class CFeedback : Form
    {
        public CFeedback()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CFeedback_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;
            label1.ForeColor = Theme.SecondaryColor;
            label3.ForeColor = Theme.SecondaryColor;
            label5.ForeColor = Theme.SecondaryColor;
            label4.ForeColor = Theme.SecondaryColor;
            label2.ForeColor= Theme.SecondaryColor;


        }
    }
}
