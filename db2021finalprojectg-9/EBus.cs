﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace db2021finalprojectg_9
{
    public partial class EBus : Form
    {
        public EBus()
        {
            InitializeComponent();
        }

        private void EBus_Load(object sender, EventArgs e)
        {
            LoadTheme();
			textBox6.Text = "0";

        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle= FlatStyle.Flat;
            button2.BackColor = Theme.PrimaryColor;
            button2.ForeColor = Color.White;
            button2.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button2.FlatStyle = FlatStyle.Flat;
            button3.BackColor = Theme.PrimaryColor;
            button3.ForeColor = Color.White;
            button3.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button3.FlatStyle = FlatStyle.Flat;
            button4.BackColor = Theme.PrimaryColor;
            button4.ForeColor = Color.White;
            button4.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button4.FlatStyle = FlatStyle.Flat;
            
        }

		private void txtModel_Leave(object sender, EventArgs e)
        {
          
		}

		private void textBox1_Leave(object sender, EventArgs e)
		{
			int year;

			if (!int.TryParse(textBox1.Text, out year) || year < 1900 || year > 2023)
			{
				textBox1.SelectAll();
				textBox1.Focus();
				errorProvider1.SetError(textBox1, "Invalid input. Please enter a four-digit number between 1900 and 2023.");
			}
			else
			{
				errorProvider1.SetError(textBox1, "");
			}
		}

		private void textBox4_Leave(object sender, EventArgs e)
		{

			// Get the current text in the textbox
			string text = textBox4.Text;

			// Check if the input is not null or empty
			if (!string.IsNullOrEmpty(text))
			{
				// Check if the input is numeric
				if (!int.TryParse(text, out _))
				{
					textBox4.SelectAll();

					// Show an error message using error provider 4 and set the focus on the textbox
					errorProvider4.SetError(textBox4, "Only numeric values are allowed.");
					textBox4.Focus();
				}
				else
				{
					// Clear any error message using error provider 4
					errorProvider4.SetError(textBox4, "");
				}
			}
			else
			{
				textBox4.SelectAll();

				textBox4.Focus();
				errorProvider4.SetError(textBox4, "Empty input is nott allowed.");

			}
		}

		private void textBox3_Leave(object sender, EventArgs e)
		{

			// Check if the entered text is a valid number
			if (!int.TryParse(textBox3.Text, out _))
			{
				textBox3.SelectAll();

				errorProvider3.SetError(textBox3, "Please enter only numeric values");
				textBox3.Focus();
			}
			else
			{
				errorProvider3.SetError(textBox3, "");
			}

		}

		private void textBox3_TextChanged(object sender, EventArgs e)
		{
			textBox6.Text = textBox3.Text;

		}

		private void textBox4_TextChanged(object sender, EventArgs e)
		{


			// Try to parse the value of textBox3 and textBox4 as integers
			int num1, num2;
			bool isNum1Valid = int.TryParse(textBox3.Text, out num1);
			bool isNum2Valid = int.TryParse(textBox4.Text, out num2);

			// If both values are valid, update the text of textBox6 to their sum
			if (isNum1Valid && isNum2Valid)
			{
				int sum = num1 + num2;
				textBox6.Text = sum.ToString();
			}
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.AutoResizeColumns();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Bus", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
