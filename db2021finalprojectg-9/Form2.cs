﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace db2021finalprojectg_9
{
    public partial class Form2 : Form
    {
        private Button currentButton;
        private Random random;
        private int tempIndex;
        int len = 0;
        public string text;
        private Form activeForm;
        public Form2()
        {
            InitializeComponent();
            random = new Random();

        }

        private void DisableButton1()
        {

            foreach (Control previousBtn in panelMenu.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = Color.FromArgb(51, 51, 76);
                    previousBtn.ForeColor = Color.Gainsboro;
                    previousBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
        }
        private void ActivateButton(object btnSender)
        {
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    DisableButton1();
                    Color color = SelectThemeColor();
                    currentButton = (Button)btnSender;
                    currentButton.BackColor = color;
                    panelLogo.BackColor = color;
                    panel11.BackColor = color;
                    currentButton.ForeColor = Color.White;
                    currentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    Theme.PrimaryColor = color;
                }
            }
        }
        private Color SelectThemeColor()
        {
            int index = random.Next(Theme.ColorList.Count);
            while (tempIndex == index)
            {
                index = random.Next(Theme.ColorList.Count);
            }
            tempIndex = index;
            string color = Theme.ColorList[index];
            return ColorTranslator.FromHtml(color);
        }

        private void btnReports_Click(object sender, EventArgs e)
        {

        }

        private void btnAttendance_Click(object sender, EventArgs e)
        {

        }

        private void btnMaintainance_Click(object sender, EventArgs e)
        {

        }

        private void btnLuggage_Click(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        private void btnAssignDuty_Click(object sender, EventArgs e)
        {

        }

        private void btnTickets_Click(object sender, EventArgs e)
        {

        }

        private void btnFeedback_Click(object sender, EventArgs e)
        {

        }

        private void btnSchedule_Click(object sender, EventArgs e)
        {

        }

        private void btnRoute_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnBus_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ABus(), sender);
            label2.Text ="BUS INFORMATION";
        }

        private void btnSchedule_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new ASehedule(), sender);
            label2.Text = "CURRENT SCHEDULES ";
        }


        private void btnMaintainance_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new AMaintainance(), sender);
            label2.Text = " BUS MAINTAINANCE";
        }

        private void btnReports_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new AReports(), sender);
            label2.Text = "REPORTS ANALYSIS";
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new ASearching(), sender);
            label2.Text = "SEARCHING";
        }

        private void btnFeedback_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new AFeedback(), sender);
            label2.Text = "CUSTOMER FEEDBACK";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           if (len < text.Length)
            {
                lblAni.Text = lblAni.Text + text.ElementAt(len);
                len++;
            }
            else
            {
                timer1.Stop();
            }
           

        }
        private void OpenChildForm(Form childForm, object btnSender)
        {
            if (activeForm != null)
                activeForm.Close();
            ActivateButton(btnSender);
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelChild.Controls.Add(childForm);
            this.panelChild.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            text = lblAni.Text;
            lblAni.Text = "";
            timer1.Start();
        }
       

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = "MANAGE USERS";
            OpenChildForm(new AAddEmployee(), sender);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (activeForm != null)
                activeForm.Close();
            Reset();
        }
        private void Reset()
        {
            DisableButton1();
            label2.Text = "HOME";

        }

        private void Form2_Resize(object sender, EventArgs e)
        {
            if (this.Width < 600) // adjust font size if the form width is less than 400 pixels
            {
                lblAni.Visible = false;
            }
            else
            {
                lblAni.Visible = true;

            }

        }
    }
}
