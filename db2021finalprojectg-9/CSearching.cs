﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace db2021finalprojectg_9
{
    public partial class CSearching : Form
    {
        public CSearching()
        {
            InitializeComponent();
        }

        private void CSearching_Load(object sender, EventArgs e)
        {
            LoadTheme();
        }
        private void LoadTheme()
        {
            button1.BackColor = Theme.PrimaryColor;
            button1.ForeColor = Color.White;
            button1.FlatAppearance.BorderColor = Theme.SecondaryColor;
            button1.FlatStyle = FlatStyle.Flat;

            label1.ForeColor = Theme.SecondaryColor;
            label3.ForeColor = Theme.SecondaryColor;


        }

		private void txtId_Validating(object sender, CancelEventArgs e)
        {
			
				if (Regex.IsMatch(txtId.Text, @"^\S+$"))
				{
                txtId.SelectAll();

                errorProvider1.SetError(txtId, "Spaces are not allowed.");

                txtId.Focus();
            }
			

		}

        private void button1_Click(object sender, EventArgs e)
        {
            //searching
        }
    }
}
